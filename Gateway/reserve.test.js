//const sum = require('./controller');

var supertest = require("supertest");
var should = require("should");

// This agent refers to PORT where program is runninng.

var app = supertest.agent("http://localhost:3000");

// UNIT test begin

describe("SAMPLE unit test",function(){

    // #1 should return home page

    it("should return home page",function(done){

        app
            .post("/reserve-room")
            .expect("Content-type",/json/)
            .expect(200) // THis is HTTP response
            .end(function(err,res){
                // HTTP status should be 200
                res.status.should.equal(200);
                // Error key should be false.
                res.body.error.should.equal(false);
                done();
            });
    });

});