const mongoose = require('mongoose');

const ReservationSchema = mongoose.Schema({
    user:{type: String, required: true},
    room: {type: String, required: true},
    status:{type:String, required:true, enum:["RESERVED", "PENDING_APPROVAL"]},
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Reservation', ReservationSchema);