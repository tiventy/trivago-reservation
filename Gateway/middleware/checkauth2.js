module.exports=(req, res, next)=>{
    try {
        if ( req.headers.authorization!== process.env.API_KEY2) {
            return res.status(401).json({status: 'error'});
        }
        next();

    }catch (error) {
        return res.status(404).json({
            message: "authentication failed"
        });
    }
};

