const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const checkauthorization = require('./middleware/checkauth');
const checkauthorization2 = require('./middleware/checkauth2');
const dotenv = require('dotenv');

    dotenv.config();

const url = 'mongodb://127.0.0.1:27017/Book-api';

mongoose.connect(url);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.on('open', function () {
    console.log('connected');
});

app.use(bodyparser.json());
const controller = require('./controller');

app.post("/reserve-room",checkauthorization,controller.reserveRoom);
app.post("/update-points",checkauthorization2, controller.updateUserPoints);

app.listen(7777, ()=>{
    console.log("app running on 7777 for orders")
});