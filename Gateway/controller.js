const model = require('./model');
const cote = require('cote');
const userService = 'User Service';
const roomService = 'Room Service';
const userClient = new cote.Requester({name: 'Client', key: userService});
const roomClient = new cote.Requester({name: 'Client', key: roomService});

module.exports.reserveRoom = function (req, res) {
    //reserve room
    const async = require('async');
    async
        .waterfall([
            (callback) =>
                getData(req.body, callback),
            (resp, callback) =>
                reserveRoom(req.body, resp.points, resp.user.bonus >= resp.points, callback)
        ], (err, result, enough) => {
            if (err)
                return res.json(err);
            new model({
                user: req.body.user,
                room: req.body.room,
                status: enough ? 'RESERVED' : 'PENDING_APPROVAL'
            }).save();
            res.json({success: true, message: enough ? 'RESERVED' : 'PENDING_APPROVAL'});
        });
};

module.exports.updateUserPoints = function (req, res) {
    userClient.send({type: 'update-user-points', id: req.body.id, points: req.body.points}, (resp) => {
        // if (!resp) return res.json({});
        // if(!resp.success)
        return res.json(resp);

    });
};

let getData = (body, callback) => {
    const async = require('async');
    async
        .parallel({
            points: (cb) => {
                console.log(`Sending request to ${roomService}@check-room`);
                roomClient
                    .send({type: 'check-room', id: body.room},
                        (resp) => {
                            console.log(`Response from ${roomService}@check-room`, resp);
                            if (resp && resp.success)
                                cb(null, resp.points);
                            else cb(resp);
                        });
            },
            user: (cb) => {
                console.log(`Sending request to ${userService}@get-user`);
                userClient
                    .send({type: 'get-user', id: body.user},
                        (resp) => {
                            console.log(`Response from ${userService}@get-user`, resp);
                            if (resp && resp.success)
                                cb(null, resp.data);
                            else cb(resp);
                        });
            }
        }, callback)
};

//
let reserveRoom = (body, points, enoughPoints, callback) => {
    const async = require('async');
    async
        .parallel({
            points: (cb) => {
                console.log(`Request to ${roomService}@reserve-room`);
                roomClient
                    .send({type: 'reserve-room', id: body.room},
                        (resp) => {
                            console.log(`Response from ${roomService}@reserve-room`, resp);
                            if (resp && resp.success)
                                cb(null, resp);
                            else cb(resp);
                        });
            },
            user: (cb) => {
                if (!enoughPoints) {
                    cb();
                    return;
                }
                console.log(`Response from ${userService}@update-user-points`, cb);
                userClient
                    .send({type: 'update-user-points', id: body.user, points: -points},
                        (resp) => {
                            console.log(`Response from ${userService}@update-user-points`, resp);
                            if (resp && resp.success)
                                cb(null, resp);
                            else cb(resp);
                        });
            }
        }, callback)
};
