
const mongoose = require('mongoose');
const { DB_URI } = require('./config');


const url = 'mongodb://localhost:27017/Book-api';



mongoose.connect(url);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.on('open', function () {
    console.log('connected to database');
});


const cote = require('cote');
const service = new cote.Responder({ name: 'User Service', key: 'User Service' });

const controller = require('./controller');

service.on('get-user', controller.getUser);
service.on('update-user-points', controller.updatePoints);