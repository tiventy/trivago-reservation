
const mongoose = require('mongoose');


const customerSchema = mongoose.Schema({
    name:{type: String, required: true},
    role: {type: String, required: true},
    bonus: {type: Number, required: false},
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('customer', customerSchema);
let customers = [
    {
        name: "john figo",
        role: "user",
        bonus: 10
    },
    {
        name: "helen joe",
        role: "user",
        bonus: 20
    },
    {
        name: "rio helm",
        role: "user",
        bonus: 30
    },
];

mongoose.model('customer')
    .countDocuments()
    .then(count=>{
        if(count)
            return;
        customers.forEach(x=> new mongoose.model('customer') (x).save());
    });