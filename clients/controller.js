const model = require('./model');

module.exports.getUser = function (body, callback) {
    model.findById(body.id).then((customer) => {
        if (customer) {
            callback({success: true, data: customer});
        }
        else {
            callback({success: false, message:"not found" });
        }
    }).catch((err) => {
        callback({success: false,message:"error occurred", error: err.message});
    })
};

module.exports.updatePoints = function (body, callback) {
    model.findById(body.id)
        .then(customer => {
            if (customer) {
                customer.bonus += Number(body.points);
                customer.save();
                callback({success: true, message: `new point is ${customer.bonus}`});
            }
            else {
                callback({success: false, message:"not found" });
            }
        }).catch((err) => {
        callback({success: false,message:"error occurred", error: err.message});
    });
};