# Trivago-reservation

# Features

 * WebSocket API(cote)
 * Middlewares (checkauth)
 * Unit Test with Jest, Logger
 * CI and CD, Jenkins & Docker Build.

# Installation
 * install dependencies
 * npm install

# Running the tests
 * run all tests
 * npm test

# Build and Run
 * serve with hot reload at localhost:7777
 * npm start

# Docker support
* Build docker image
* bash Dockerbuild.sh
* Run docker container
* docker run -d --name micro-websocket -p 3030:3030 jsboilerplates/micro-websocket


