const model = require('./model');

module.exports.checkRoom = function (body, callback) {
    model.findById(body.id)
        .then(room => {
            if (!room)
                callback({success: false, message: "room not found"});
            else if (room.available_amount)
                callback({
                    success: true,
                    message: "room is available, " + room.available_amount,
                    points: room.required_points
                });
            else
                callback({success: false, message: "room is unavailable"});
        }).catch((err) => {
            console.log(err.message);
        callback({success: false, message: 'An error occurred'});
    });
};

module.exports.reserveRoom = function (body, callback) {
    model.findById(body.id)
        .then(room => {
            if (room) {
                --room.available_amount;
                room.save();
                callback({success: true, data: room});
            }
            else {
                callback({success: false});
            }
        }).catch((err) => {
        callback({success: false, error: err.message});
    });
};