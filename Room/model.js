const mongoose = require('mongoose');

const RoomSchema = mongoose.Schema({
    name:{type: String, required: true, unique:true},
    available_amount: {type:Number, required: true},
    required_points: {type:Number, required: true},
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Room', RoomSchema);

let rooms = [
    {
        name: "Economy Single Room",
        available_amount: 10,
        required_points: 260
    },
    {
        name: "Economy Double Room",
        available_amount: 100,
        required_points: 100
    },
    {
        name: "Standard Single Room",
        available_amount: 10,
        required_points: 260
    },
];

//Seed sample model if none exists in the database, no need for endpoint to create model as this is just a sample

mongoose.model('Room')
.countDocuments()
.then(count=>{
    if(count)
        return;
    rooms.forEach(x=> new mongoose.model('Room') (x).save());
});