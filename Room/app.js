
const mongoose = require('mongoose');

const url = 'mongodb://127.0.0.1:27017/Book-api';

mongoose.connect(url);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.on('open', function () {
    console.log('connected to database');
});


const cote = require('cote');
const service = new cote.Responder({ name: 'Room Service', key: 'Room Service' });
const controller = require('./controller');

service.on('check-room', controller.checkRoom);
service.on('reserve-room', controller.reserveRoom);
